# TrY #

Automation of the TeX/LaTeX compilation.

Author: Ajabu Tex <ajabutex@gmail.com>  
Version: 4.0 - Release 2013-2014

* * *

Program for the automation of the compilation of (La)TeX documents.
Scans the commands from the comment lines of the document and executes
them automatically.

Great attention is paid to the flexibility and ease of use.


## Limitations ##

TrY is written for Linux and works on Linux. I didn't test the program
on Windows.  **CAUTION: I don't know if the program properly works on
Windows.  I think not.**


##  Contents of the package ##

* try.nw: the source code for the program and the documentation
* try: the python program
* try-doc.pdf: the documentation
* README: this file
* COPYING: GNU GPL license informations


## How to compile it ##

The source file (`try.nw`) is written in [noweb][*], a tool by
Norman Ramsey for [_Literate Programming_][**]. To extract and
compile the program and the documentation use the following commands:

program

    $ notangle -Rtry try.nw > try
    $ chmod +x try

documentation

    $ noweave -index -latex try.nw > try-doc.tex
    $ pdflatex try-doc.tex
    $ pdflatex try-doc.tex
    $ pdflatex try-doc.tex


## Installation ##

Make the file `try` executable then copy it into the `bin` directory of
your system, e.g.:

    $ chmod +x try 
    $ sudo cp try /usr/bin/try


## Use ##

Open a terminal and use the following command line:

    $ try [file [--log] [--safe] [--verbose] | --help | --version]
    $ try [file [-l] [-s] [-v] | -h | -V]

More on this in `try-doc.pdf`


## Contributing ##

If you want to fork the project or if you want to send a pull request,
or simply download the latest version go to

<https://bitbucket.org/ajabutex/try>
    

## License ##

This material is subject to the GPL - GNU General Public License. See

<http://www.gnu.org/licenses/gpl.txt>

for the details of that license.


Happy TeXing.

[*]: http://www.cs.tufts.edu/~nr/noweb/
[**]: http://www.literateprogramming.com/
